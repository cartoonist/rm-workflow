# coding=utf-8

"""Variation graph read mapping workflow.

This Snakefile requires a configuration file having at least these entries:

    ---
    datasets:
      - "dataset1"
      - "dataset2"
      - ...

    regions:
      dataset1:
        - region1
        - region2
        - ...
      dataset2:
        - region3
        - ...
      ...

    # Either specify input genome graph for each region of datasets in `vg`/`gfa` format:
    graph:
      dataset1:
        region1: "/path/to/its_graph.vg"
        region2: "/path/to/another_graph.vg"
        ...
      dataset2:
        region3: "/path/to/another_graph.vg"
        ...
      ...
    # Or use region and/or dataset wildcards:
    region_graphs: "/path/to/{dataset}/{region}.vg"

    # Either specify input reads for each dataset:
    reads:
      dataset1: "/path/to/dataset1/reads.fastq"
      dataset2: "/path/to/dataset2/reads.fastq"
      ...

    # Or use wildcard.
    dataset_reads: "/path/to/{dataset}/reads.fastq"

    # Or by not specifying neither of them it will be simulated from graph based on
    # ... parameters specified in `sim` section here.

    targets:
      - "/path/to/TARGET_A"
      - "/another/path/to/TARGET_B"
      - ...

    ## Required parameters for simulation (if simulation is implied by the `targets`)
    sim:
      # Read simulator: 'ggsim'
      tool: "ggsim"
      # Number of reads to be simulated
      num_reads: 1000
      # Length of the simulated reads
      read_length: 50

    ## Required parameters for FASTQ conversion (if conversion is implied by the `targets`)
    fastq:
      # FASTQ converter: 'seq2any'
      tool: "seq2any"

    ## Required parameters for distance index merge (if merge is implied by the `targets`)
    dindex_merge:
      # [MULTIPLE] Distance ranges
      distances:
        - [1, 100]
        - [100, 200]

    ## Required parameters for mapping (if mapping is implied by the `targets`)
    map:
      # Read mapper: 'psikt', 'ga-psi' (GraphAligner/PSI),
      # ... 'ga-mem' (GraphAligner/MEM), 'ga-mum' (GraphAligner/MUM),
      # ... 'ga-min' (GraphAligner/Minimizer), 'vg-map' (vg/map)
      tool: "psikt"

      ### Required options for PSI (`psikt` or `ga-psi`)

      # [MULTIPLE] Seed length
      seed_length:
        - "30"

      ### Required options for GraphAligner/MXM

      # [MULTIPLE] Seed length
      seed_length:
        - "20"
      # [MULTIPLE] MXM count -- see `--seeds-mxm-count` options for `GraphAligner`
      mxm_count:
      - "0"  # should have a non-zero value

      ### Required options for GraphAligner/Minimizer

      # [MULTIPLE] Seed length
      seed_length:
        - "19"
      # [MULTIPLE] Minimizer parameters for different minimizer lengths (`seed_length`)
      minimizer:
        19:
          - density: "-1"  # should have a non-zero value (-1 for all)

      ### No required option for vg/map
    ...

where `TARGET_A` or `TARGET_B` can be EXACTLY one of the final targets:

- "report.html"              for generating the experiment report.
- "{dataset}/map.done"       for mapping datasets reads to their corresponding region graphs.

or intermediate targets which skip mapping:

- "{dataset}/sim.done"           for simulating reads for each dataset from their region graphs separately.
- "{dataset}/chr1_sim.done"      for simulating reads from region `chr1` of each dataset.
- "{dataset}/dindex_merge.done"  for merging distance indices.

The `{dataset}` pattern will be expanded for all datasets specified in
configuration file or can be manually stated; e.g "/path/to/dataset1/map.done".
In addition, whole genome pseudo name "wg" can be used as region name in order
to simulate the reads from the whole genome graph by merging all region graphs
before simulation. This pseudo name can be configured too ("wg_pname").

There is also a sample configuration file in the root directory.

For more information about the workflow see README file.
"""

__author__ = "Ali Ghaffaari"
__email__ = "ali.ghaffaari@mpi-inf.mpg.de"
__organization__ = "Max-Planck Institut fuer Informatik"
__license__ = "MIT"
__version__ = "v1.3.2"


# Using `bash` for shell commands.
shell.executable("/bin/bash")
# Check for Snakemake minimum version.
snakemake.utils.min_version("3.9.0")

configfile: "config.yml"

WORKFLOW_PREFIX = "https://bitbucket.org/cartoonist/rm-workflow/raw/" + __version__
WRAPPERS_PREFIX = "https://github.com/cartoonist/snakemake-wrappers/raw/develop"
BLOB_TOKEN = 'src'  # GitHub: 'blob', BitBucket: 'src', Local: ''

# Fetch targets
TARGETS = expand(config["targets"], dataset=config["datasets"])

##  Rules
rule all:
    input:
        TARGETS

include:
    WORKFLOW_PREFIX + "/rules/rm.rules"

include:
    WORKFLOW_PREFIX + "/rules/dindex.rules"

include:
    WORKFLOW_PREFIX + "/rules/report.rules"
